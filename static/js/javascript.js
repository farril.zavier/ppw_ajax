var favoritedIcon = "/static/img/favorited.png";
var defaultIcon = "/static/img/default.png";

function addToFavorites(caller, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '/add/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {book_id: id},
        success: function (result) {
            $(caller).attr('src', favoritedIcon);
            $("#counter").replaceWith('<span id="counter">' + result.counter + '</span>');
            loadAgain();
        },
        error: function () {
            alert("Cannot obtain data from server!")
        }
    });
}

function removeFromFavorites(caller, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '/remove/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {book_id: id},
        success: function (result) {
            $(caller).attr('src', defaultIcon);
            $("#counter").replaceWith('<span id="counter">' + result.counter + '</span>');
            loadAgain();
        },
        error: function () {
            alert("Cannot obtain data from server!")
        }
    });
}

function onHover(caller) {
    $(caller).attr('src', favoritedIcon);
}

function offHover(caller) {
    $(caller).attr('src', defaultIcon);
}

function loadAgain() {
    $.get("/api/",
        function (data) {
            var pageUpdate = jQuery(data);
            var counterUpdate = pageUpdate.find("#counter").html();
            var favoritedUpdate = pageUpdate.find("#table").html();
            $("#counter").html(counterUpdate);
            $("#table").html(favoritedUpdate);
        }
    );
}

function request(query) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: "/api/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {query: query},
        success: function (result) {
            loadAgain();
            console.log("Success!");
        },
        error: function () {
            alert("Failed!")
        }
    });
}

$("#programming").click(function () {
    request("programming");
});

$("#english").click(function () {
    request("english");
});

$("#quilting").click(function () {
    request("quilting");
});
