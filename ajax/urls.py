from django.urls import path, include
from .views import index, add_to_favorites, remove_from_favorites, api, login, logout

urlpatterns = [
    path('', index, name='index'),
    path('api/', api, name='api'),
    path('add/', add_to_favorites, name='add_to_favorites'),
    path('remove/', remove_from_favorites, name='remove_from_favorites'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
]