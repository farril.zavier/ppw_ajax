from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import FavoriteBooks

# Create your tests here.

class UnitTest(TestCase):
    def test_url_main_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_main_page_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_add_favorite(self):
        FavoriteBooks.objects.create(book_id='TEST')

        counting_all_status_message = FavoriteBooks.objects.all().count()
        self.assertEqual(counting_all_status_message, 1)
