from django.db import models

# Create your models here.
class FavoriteBooks(models.Model):
    book_id = models.CharField(max_length=300, unique=True, primary_key=True)
