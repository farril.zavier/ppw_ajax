from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import logout as auth_logout
from .models import FavoriteBooks
import requests

# Create your views here.
response = {}
base_url = "https://www.googleapis.com/books/v1/volumes?q="
url = ""

def index(request):
    if request.user.is_authenticated:
        base = 'base_logged_in.html'
        request.session['user'] = request.user.username
        request.session['email'] = request.user.email
        response['base'] = base
        response['navItem'] = 'index'
        global url
        url = base_url + "quilting"
        favorited_book = FavoriteBooks.objects.all()
        counter = FavoriteBooks.objects.count()
        favorited_book_id = [book.book_id for book in favorited_book]
        books_json = requests.get(url).json()
        # response['books'] = books_json['items']
        # response['favorited'] = favorited_book_id
        # response['counter'] = counter
        request.session['url'] = url
        request.session['books'] = books_json['items']
        request.session['favorited'] = favorited_book_id
        request.session['counter'] = counter
        return render(request, 'index.html', response)
    else:
        base = 'base.html'
        response['base'] = base
        response['navItem'] = 'login'
        return render(request, 'login.html', response)

def api(request):
    if request.method == 'POST':
        query = request.POST['query']
        global url
        url = base_url + query
    favorited_book = FavoriteBooks.objects.all()
    counter = FavoriteBooks.objects.count()
    favorited_book_id = [book.book_id for book in favorited_book]
    books_json = requests.get(url).json()
    # response['books'] = books_json['items']
    # response['favorited'] = favorited_book_id
    # response['counter'] = counter
    request.session['url'] = url
    request.session['books'] = books_json['items']
    request.session['favorited'] = favorited_book_id
    request.session['counter'] = counter
    return render(request, 'index.html', response)

def add_to_favorites(request):
    if request.method == 'POST':
        book_id = request.POST['book_id']
        FavoriteBooks.objects.create(book_id=book_id)
        counter = FavoriteBooks.objects.count()
        favorited_book = FavoriteBooks.objects.all()
        favorited_book_id = [book.book_id for book in favorited_book]
        books_json = requests.get(request.session['url']).json()
        request.session['books'] = books_json['items']
        request.session['favorited'] = favorited_book_id
        request.session['counter'] = counter
        return JsonResponse({'counter': request.session['counter']})

def remove_from_favorites(request):
    if request.method == 'POST':
        book_id = request.POST['book_id']
        FavoriteBooks.objects.get(book_id=book_id).delete()
        counter = FavoriteBooks.objects.count()
        favorited_book = FavoriteBooks.objects.all()
        favorited_book_id = [book.book_id for book in favorited_book]
        books_json = requests.get(request.session['url']).json()
        request.session['books'] = books_json['items']
        request.session['favorited'] = favorited_book_id
        request.session['counter'] = counter
        return JsonResponse({'counter': request.session['counter']})

def login(request):
    if request.user.is_authenticated:
        base = 'base_logged_in.html'
    else:
        base = 'base.html'
    response['base'] = base
    response['navItem'] = 'login'
    return render(request, 'login.html', response)

def logout(request):
    base = 'base.html'
    response['base'] = base
    response['navItem'] = ''
    FavoriteBooks.objects.all().delete()
    request.session.flush()
    auth_logout(request)
    return render(request, 'logout.html', response)
